# WPezPlugins: Hubspot Newsletter Form AJAX Form for AU

### This code is shared simply as a (working) reference point. Obviously, given the copy, you'd have to make some changes.

The ideal place to copy / fork, open an issue, etc. would be here:

https://gitlab.com/WPezPlugins/wpez-hubspot-newsletter-form-ajax-form


_Disclaimer: The copy / verbiage used here may or may not be legally airtight. Please do not assume anything other than you'll need to do your own (legal) vetting._
